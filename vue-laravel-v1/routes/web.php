<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/peliculas','App\Http\Controllers\AdministradorController');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/peliculas', [
   'uses' => 'App\Http\Controllers\PeliculasController@index',
    'as' => 'peliculas.index'
]);

Route::get('/populares', [
    'uses' => 'App\Http\Controllers\PeliculasController@popular',
    'as' => 'peliculas.popular'
]);

Route::get('/detalles/{id}', [
    'uses' => 'App\Http\Controllers\PeliculasController@parametres',
    'as' => 'peliculas.details'
]);




Route::group(['middleware' => 'auth'],function(){
    Route::get('/detalles/{id}/comprar', [
        'uses' => 'App\Http\Controllers\PeliculasController@buy',
        'as' => 'peliculas.buy'
    ]);

    Route::get('/detalles/{id}/ticket', [
        'uses' => 'App\Http\Controllers\PeliculasController@ticket',
        'as' => 'peliculas.ticket'
    ]);
});
