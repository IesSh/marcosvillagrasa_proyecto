<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new \App\Models\User([
            'name'=>'MarcosAdmin',
            'email'=>'marcosvillaara@gmail.com',
            'password'=>'$2y$10$k12BAeONASscKiaF9N0QCu8yqnjnqvmDmtAeb5qgO1y50R0CfkvCm',
            'role_id'=>1,
        ]);
        $user -> save();
    }
}
