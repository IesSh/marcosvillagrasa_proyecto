<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PeliculasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Fate/Stay Night: Heavens Feel - III. Spring Song',
            'genero'=>'Fantasía/Drama',
            'idioma'=>'Japonés Subtitulado',
            'horas'=>'19:00',
            'trailer'=>'https://www.youtube.com/embed/GTA20VJeyYk',
            'sinopsis'=>'Una batalla violenta entre magos en la que siete masters y sus servants luchan por el Santo Grial,
            un artefacto mágico que puede otorgarle al vencedor cualquier deseo. Hace casi 10 años,
            la batalla final de la Cuarta Guerra del Santo Grial causó estragos en la ciudad de Fuyuki y se cobró más de 500 vidas,
            dejando la ciudad devastada. Shirou Emiya, un sobreviviente de esta tragedia, aspira a convertirse en un héroe de la justicia como su salvador y padre adoptivo,
            Kiritsugu Emiya. A pesar de ser solo un estudiante, Shirou es arrojado a la Quinta Guerra del Santo Grial cuando accidentalmente ve una
            batalla entre servants en la escuela y convoca a su propio servant, Saber. Cuando una sombra misteriosa comienza una ola de asesinatos en la ciudad de Fuyuki,
            Shirou se alía con Rin Toosaka, una compañera participante en la Guerra del Santo Grial, para detener las muertes de innumerables personas.
            Sin embargo, los sentimientos de Shirou por su amiga cercana Sakura Matou lo llevan a profundizar en los oscuros secretos que rodean la guerra y las familias involucradas.',
            'imagen'=>'fate.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Senderos de gloria',
            'genero'=>'Bélico/Drama',
            'idioma'=>'Español',
            'horas'=>'20:00',
            'trailer'=>'https://www.youtube.com/embed/byS2AYtv130',
            'sinopsis'=>'Ambientada en la Primera Guerra Mundial, en la Francia de 1916 tratando de resistir a la invasión alemana,
            Senderos de gloria se articula en dos partes y un epílogo: La orden y la toma de una colina comandada por el coronel Dax (Kirk Douglas) y el juicio a tres soldados
            a los que los altos mandos pretenden ejecutar como escarmiento ante el fracaso de la misión a causa de lo que consideran una actitud de cobardía.
            Ya lo dicen unos diálogos afilados en los que los autores despliegan un sarcasmo helador. Los soldados, además, son despojados de su identidad:
            No serán ejecutados por sus acciones directas,
            sino como representantes escogidos casi al azar, como simple proporción numérica de toda la compañía.',
            'imagen'=>'senderos_de_glori.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Harry Potter y la piedra filosofal',
            'genero'=>'Fantasía/Aventura',
            'idioma'=>'Español',
            'horas'=>'21:00',
            'trailer'=>'https://www.youtube.com/embed/WE4AJuIvG1Y',
            'sinopsis'=>'Harry Potter se ha quedado huérfano y vive en casa de sus abominables tíos y el insoportable primo Dudley. Harry se siente muy triste y solo,
            hasta que un buen día recibe una carta que cambiará su vida para siempre. En ella le comunican que ha sido aceptado como alumno en el Colegio Hogwarts de Magia.
            A partir de ese momento, la suerte de Harry da un vuelco espectacular. En esa escuela tan especial aprenderá encantamientos,
            trucos fabulosos y tácticas de defensa contra las malas artes. Se convertirá en el campeón escolar de Quidditch, especie de fútbol aéreo que se juega montado sobre escobas,
            y hará un puñado de buenos amigos... aunque también algunos temibles enemigos. Pero, sobre todo, conocerá los secretos que le permitirán cumplir su destino.
            Pues, aunque no lo parezca a primera vista, Harry no es un chico común y corriente: ¡es un verdadero mago!',
            'imagen'=>'harrypotter.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Harry Potter y la cámara secreta',
            'genero'=>'Fantasía/Aventura',
            'idioma'=>'Español',
            'horas'=>'17:00',
            'trailer'=>'https://www.youtube.com/embed/C8CL5TbiFwY',
            'sinopsis'=>'Tras derrotar una vez más a lord Voldemort, su siniestro enemigo en Harry Potter y la piedra filosofal, Harry espera impaciente en casa de sus insoportables tíos el inicio del segundo curso del Colegio Hogwarts de Magia. Sin embargo, la espera dura poco, pues un elfo aparece en su habitación y le advierte que una amenaza mortal se cierne sobre la escuela. Así pues, Harry no se lo piensa dos veces y, acompañado de Ron, su mejor amigo, se dirige a Hogwarts en un coche volador. Pero ¿puede un aprendiz de mago defender la escuela de los malvados que pretenden destruirla? Sin saber que alguien había abierto la Cámara de los Secretos, dejando escapar una serie de monstruos peligrosos, Harry y sus amigos Ron y Hermione tendrán que enfrentarse con arañas gigantes, serpientes encantadas, fantasmas enfurecidos y, sobre todo, con la mismísima reencarnación de su más temible adversario.
',
            'imagen'=>'harry_potter_camara.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Harry Potter y el cáliz de fuego',
            'genero'=>'Fantasía/Aventura',
            'idioma'=>'Español',
            'horas'=>'16:00',
            'trailer'=>'https://www.youtube.com/embed/zKTlWaaOgv4',
            'sinopsis'=>'La cuarta parte de la serie del niño mago comienza con la Copa Internacional de Quidditch. Cuenta también el inicio de la atracción por Cho Chang y otro año de magia, en el que una gran sorpresa obligará a Harry a enfrentarse a muchos desafíos temibles. También habrá un torneo de magia para tres escuelas, y el temido regreso de "Aquel-que-no-debe-ser-nombrado".',
            'imagen'=>'harry_potter3.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'El Señor de los Anillos: las dos torres',
            'genero'=>'Fantasía/Aventura',
            'idioma'=>'Español',
            'horas'=>'17:00',
            'trailer'=>'https://www.youtube.com/embed/f9W1l7E5bHg',
            'sinopsis'=>'Tras la disolución de la Compañía del Anillo, Frodo y su fiel amigo Sam se dirigen hacia Mordor para destruir el Anillo Único y acabar con el poder de Sauron, pero les sigue un siniestro personaje llamado Gollum. Mientras, y tras la dura batalla contra los orcos donde cayó Boromir, el hombre Aragorn, el elfo Legolas y el enano Gimli intentan rescatar a los medianos Merry y Pipin, secuestrados por los orcos de Mordor. Por su parte, Saurón y el traidor Sarumán continúan con sus planes en Mordor, a la espera de la guerra contra las razas libres de la Tierra Media.',
            'imagen'=>'señor_anillos_torres.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'El Señor de los Anillos: la Comunidad del Anillo',
            'genero'=>'Fantasía/Aventura',
            'idioma'=>'Español',
            'horas'=>'17:00',
            'trailer'=>'https://www.youtube.com/embed/2KTjutR3mGA',
            'sinopsis'=>'En la Tierra Media, el Señor Oscuro Sauron ordenó a los Elfos que forjaran los Grandes Anillos de Poder. Tres para los reyes Elfos, siete para los Señores Enanos, y nueve para los Hombres Mortales. Pero Saurón también forjó, en secreto, el Anillo Único, que tiene el poder de esclavizar toda la Tierra Media. Con la ayuda de sus amigos y de valientes aliados, el joven hobbit Frodo emprende un peligroso viaje con la misión de destruir el Anillo Único. Pero el malvado Sauron ordena la persecución del grupo, compuesto por Frodo y sus leales amigos hobbits, un mago, un hombre, un elfo y un enano. La misión es casi suicida pero necesaria, pues si Sauron con su ejército de orcos lograra recuperar el Anillo, sería el final de la Tierra Media.',
            'imagen'=>'señor_anillos_comunidad.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'La naranja mecánica',
            'genero'=>'Ciencia ficción/Crimen',
            'idioma'=>'Español',
            'horas'=>'14:00',
            'trailer'=>'https://www.youtube.com/embed/A1eC4pG8rC0',
            'sinopsis'=>'Gran Bretaña, en un futuro indeterminado. Alex (Malcolm McDowell) es un joven muy agresivo que tiene dos pasiones: la violencia desaforada y Beethoven. Es el jefe de la banda de los drugos, que dan rienda suelta a sus instintos más salvajes apaleando, violando y aterrorizando a la población. Cuando esa escalada de terror llega hasta el asesinato, Alex es detenido y, en prisión, se someterá voluntariamente a una innovadora experiencia de reeducación que pretende anular drásticamente cualquier atisbo de conducta antisocial.',
            'imagen'=>'naranja.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Tenet',
            'genero'=>'Acción/Ciencia ficción',
            'idioma'=>'Español',
            'horas'=>'16:00',
            'trailer'=>'https://www.youtube.com/embed/CdRL6o8z-2A',
            'sinopsis'=>'Armado con tan solo una palabra –Tenet– el protagonista de esta historia deberá pelear por la supervivencia del mundo entero en una misión que le lleva a viajar a través del oscuro mundo del espionaje internacional, y cuya experiencia se desdoblará más allá del tiempo lineal.',
            'imagen'=>'tenet.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Kimi no Na wa',
            'genero'=>'Animación/Romance',
            'idioma'=>'Japonés Subtitulado',
            'horas'=>'17:00',
            'trailer'=>'https://www.youtube.com/embed/yPPaLgSXYlM',
            'sinopsis'=>'Taki y Mitsuha descubren un día que durante el sueño sus cuerpos se intercambian, y comienzan a comunicarse por medio de notas. A medida que consiguen superar torpemente un reto tras otro, se va creando entre los dos un vínculo que poco a poco se convierte en algo más romántico.',
            'imagen'=>'your_Name.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Avatar',
            'genero'=>'Ciencia ficción/Acción',
            'idioma'=>'Español',
            'horas'=>'16:00',
            'trailer'=>'https://www.youtube.com/embed/Xg8kYk6uHN0',
            'sinopsis'=>'Año 2154. Jake Sully (Sam Worthington), un ex-marine condenado a vivir en una silla de ruedas, sigue siendo, a pesar de ello, un auténtico guerrero. Precisamente por ello ha sido designado para ir a Pandora, donde algunas empresas están extrayendo un mineral extraño que podría resolver la crisis energética de la Tierra. Para contrarrestar la toxicidad de la atmósfera de Pandora, se ha creado el programa Avatar, gracias al cual los seres humanos mantienen sus conciencias unidas a un avatar: un cuerpo biológico controlado de forma remota que puede sobrevivir en el aire letal. Esos cuerpos han sido creados con ADN humano, mezclado con ADN de los nativos de Pandora, los Na`vi. Convertido en avatar, Jake puede caminar otra vez. Su misión consiste en infiltrarse entre los Na`vi, que se han convertido en el mayor obstáculo para la extracción del mineral. Pero cuando Neytiri, una bella Na`vi (Zoe Saldana), salva la vida de Jake, todo cambia: Jake, tras superar ciertas pruebas, es admitido en su clan. Mientras tanto, los hombres esperan los resultados de la misión de Jake.',
            'imagen'=>'avatar.jpg',
        ]);
        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Interstellar',
            'genero'=>'Ciencia ficción/Aventura',
            'idioma'=>'Español',
            'horas'=>'16:00',
            'trailer'=>'https://www.youtube.com/embed/NqniWGlg5kU',
            'sinopsis'=>'Al ver que la vida en la Tierra está llegando a su fin, un grupo de exploradores dirigidos por el piloto Cooper (McConaughey) y la científica Amelia (Hathaway) emprende una misión que puede ser la más importante de la historia de la humanidad: viajar más allá de nuestra galaxia para descubrir algún planeta en otra que pueda garantizar el futuro de la raza humana.',
            'imagen'=>'interestellar.jpg',
        ]);

        $pelicula -> save();

        $pelicula= new \App\Models\Peliculas([
            'titulo'=>'Spider-Man: Un nuevo universo',
            'genero'=>'Animación/Acción',
            'idioma'=>'Español',
            'horas'=>'21:00',
            'trailer'=>'https://www.youtube.com/embed/qLObEn9IK20',
            'sinopsis'=>'En un universo paralelo donde Peter Parker ha muerto, un joven de secundaria llamado Miles Morales es el nuevo Spider-Man. Sin embargo, cuando el líder mafioso Wilson Fisk (a.k.a Kingpin) construye el "Super Colisionador" trae a una versión alternativa de Peter Parker que tratará de enseñarle a Miles como ser un mejor Spider-Man. Pero no será el único Spider Man en entrar a este universo, 4 versiones alternas de Spidey aparecerán y buscarán regresar a su universo antes de que toda la realidad colapse.',
            'imagen'=>'spiderman_multiverse.jpg',
        ]);

        $pelicula -> save();
    }
}
