@extends('layouts.app')

@section('content')
<div class="container">

    <img src="images/spidermanFeliz.jpg" alt="" class="border border-dark mx-auto d-block rounded">
    <h1 class="mt-5 ml-5 align-content-center">Bienvenido {{ Auth::user()->name }}, te has registrado correctamente.</h1>
</div>
@endsection
