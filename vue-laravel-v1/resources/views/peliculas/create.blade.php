@extends('layouts.app')

@section('content')
    <div class="container">

        <form method="POST" action="/peliculas">
            @csrf

            <div class="form-group row">
                <label for="titulo" class="col-md-4 col-form-label text-md-right">Nombre</label>

                <div class="col-md-6">
                    <input id="titulo" type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" required autocomplete="titulo" autofocus>
                </div>
            </div>

            <div class="form-group row">
                <label for="genero" class="col-md-4 col-form-label text-md-right">Genero</label>

                <div class="col-md-6">
                    <input id="genero" type="text" class="form-control" name="genero" value="{{ old('genero') }}" required autocomplete="genero">

                </div>
            </div>

            <div class="form-group row">
                <label for="idioma" class="col-md-4 col-form-label text-md-right">Idioma</label>

                <div class="col-md-6">
                    <input id="idioma" type="text" class="form-control" name="idioma" value="{{ old('idioma') }}" required autocomplete="genero">

                </div>
            </div>

            <div class="form-group row">
                <label for="sinopsis" class="col-md-4 col-form-label text-md-right">Sinopsis</label>

                <div class="col-md-6">
                    <input id="sinopsis" type="text" class="form-control" name="sinopsis" value="{{ old('sinopsis') }}" required autocomplete="sinopsis">
                </div>
            </div>

            <div class="form-group row">
                <label for="horas" class="col-md-4 col-form-label text-md-right">Horas</label>

                <div class="col-md-6">
                    <input id="horas" type="time" class="form-control" name="horas" value="{{ old('horas') }}" required autocomplete="horas">

                </div>
            </div>

            <div class="form-group row">
                <label for="dia" class="col-md-4 col-form-label text-md-right">Día de la semana</label>

                <div class="col-md-6">
                    <input id="dia" type="week" class="form-control" name="dia" value="{{ old('dia') }}" required autocomplete="horas">

                </div>
            </div>
            <div class="form-group row">
                <label for="trailer" class="col-md-4 col-form-label text-md-right">Trailer</label>

                <div class="col-md-6">
                    <input id="trailer" type="text" class="form-control" name="trailer" value="{{ old('trailer') }}" required autocomplete="trailer">

                </div>
            </div>

            <div class="form-group row">
                <label for="imagen" class="col-md-4 col-form-label text-md-right">Imagen</label>

                <div class="col-md-6">

                    <input accept="image/*" type="file" name="imagen" >
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Crear
                    </button>
                </div>
            </div>
        </form>



    </div>

@endsection
