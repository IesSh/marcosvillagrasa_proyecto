@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-12 card" style="background-color: #b0c8f3">
            <div>
                <a class="navbar-brand" href="{{ url('/peliculas') }}" style="color: black">
                    Películas
                </a><span class="navbar-brand">/</span>
                <a class="navbar-brand" href="{{ route('peliculas.details',$peliculas) }}" style="color: black">
                    {{ $peliculas->titulo }}
                </a><span class="navbar-brand">/</span>
                <a class="navbar-brand" href="{{ route('peliculas.buy',$peliculas) }}" style="color: black">
                    Comprar entrada
                </a>
            </div>
        </div>
        <div class="col-12 mt-2">
            <h1 class="text-center">{{$peliculas->titulo}}</h1>

            <hr>
        </div>

        <div class="col-4 ml-5">
            <img class="img-thumbnail" src="{{ URL::to('images') }}/{{ $peliculas->imagen }}"/>
        </div>
        <div class="col-4 ml-5">

        <?php

        /*Recojo la butaca comprada*/

        $compra = $_GET["butaca"];

        /*Recojo el nombre del comprador*/

        $nombre = $_GET["nombre"];

        /*Recojo el numero de palomitas*/

        $palomitas = $_GET["palomitas"];

        /*Recojo el numero de refrescos*/

        $refrescos = $_GET["refrescos"];

        /*Recojo el tipo de descuento*/

        $descuento = $_GET["descuento"];



            if ($descuento=='nada'){
                $descuentoTotal=0;
            }
            if ($descuento=='joven'){
                $descuentoTotal=1.00;
            }
            if ($descuento=='uni'){
                $descuentoTotal=1.00;
            }
            if ($descuento=='cine'){
                $descuentoTotal=0.50;
            }

            /*Recojo la tipo palomita*/

            $tipoPalomita = $_GET["tipoPalomitas"];

            if ($tipoPalomita=='peque'){
                $palomitaCantidad=3.59;
            }
            if ($tipoPalomita=='mediana'){
                $palomitaCantidad=6.59;
            }
            if ($tipoPalomita=='grande'){
                $palomitaCantidad=10.59;
            }


            /*Recojo la tipo bebida*/

            $tipoBebida = $_GET["tipoBebida"];

            if ($tipoBebida=='agua'){
                $bebidaCantidad=1.59;
            }
            if ($tipoBebida=='cocacola'){
                $bebidaCantidad=3.99;
            }
            if ($tipoBebida=='fantaN'){
                $bebidaCantidad=3.99;
            }
            if ($tipoBebida=='fantaL'){
                $bebidaCantidad=3.99;
            }
            if ($tipoBebida=='nestea'){
                $bebidaCantidad=3.99;
            }


        /*utilizo una variable estatica como un contador*/

            if ($palomitas==null||$tipoPalomita=='nada'||$palomitas==''){
                $palomitas=0;
                $palomitaCantidad=0;
            }
            if ($refrescos==null||$tipoBebida=='nada'||$refrescos==''){
                $refrescos=0;
                $bebidaCantidad=0;
            }

            $cantidadPalomitaTotal=$palomitaCantidad*$palomitas;

            $cantidadBebidaTotal=$bebidaCantidad*$refrescos;

        function pop(){
            static $cont = 0;
            $cont++;
            return $cont;
        }

        $palomitas2=0;

        /*al final del bucle la variable estatica de la funcion sera igual a el numero de palomitas*/

        for ($i=0; $i < $palomitas ; $i++) {
            $palomitas2 = pop();
        }


        /*Creo un array asociativo con el comprador y la butaca*/

        $comprado = array($nombre => $compra);

        /*Deserializo el array de butacas ocupadas para poder trabajar con el y lo guardo en una variable*/

        $ocupadasRandom = unserialize(stripslashes($_GET["ocupadas"]));

        /*Constantes con texto para mostrar*/
        define("resultadoCompra", " has comprado la butaca: ");
        define("resultadoOcupada", "La butaca esta ocupada");
        define("resultadoNada", "No has seleccionado ninguna butaca");

        /*Funciones built_in de arrays, creo un array con palomitas y refrescos*/

        $comida = array("palomitas","refrescos");

        $newComida = array_push($comida,$palomitas2,$refrescos);

        /*Mostrar array de butacas ocupadas random

            foreach ($ocupadasRandom as $key) {
                echo $key;
        */

        /*Genero un array con el numero y letra de cada butaca*/

        for ($i=1; $i <= 4; $i++) {
            for ($j=1; $j <= 4; $j++) {
                switch ($j) {
                    case 1:
                        $butaca[$i-1][$j-1] = $i."a";
                        break;
                    case 2:
                        $butaca[$i-1][$j-1] = $i."b";
                        break;
                    case 3:
                        $butaca[$i-1][$j-1] = $i."c";
                        break;
                    case 4:
                        $butaca[$i-1][$j-1] = $i."d";
                        break;
                }
            }
        }

        /*visualiza los numeros y letras de las butacas ordenadas

        for ($i=3; $i >= 0; $i--) {
            for ($j=0; $j <= 3; $j++) {
                echo $butaca[$j][$i];
            }
            echo "</br>";
        }

        */

        /*Compruebo si la butaca que se quiere comprar esta ocupada o libre, accedo a la butaca a traves del array asociativo*/
        /*Declaro una variable booleana*/
        $flag = false;

        foreach ($ocupadasRandom as $key) {
            if($key == $comprado[$nombre]){

                /*Modifico el tipo de la variable booleana y la convierto en string*/
                $flag = "true";
            }
        }

        /*Si la butaca esta ocupada entonces muestro el mensaje, sino Muestro las butacas por orden y con la imagen que le corresponde,
         tambien indico cual es la butaca que se ha comprado*/

        if($comprado[$nombre] == "numero"){

            //si dejas la opcion por defecto
            echo resultadoNada;

        }elseif ($flag == "true") {

            //si la butaca esta ocupada
            echo resultadoOcupada;

        }else{

            //si la butaca no esta ocupada

            for ($i=3; $i >= 0; $i--) {

                //este switch es para escribir las letras que sirven como coordenadas

                switch ($i) {
                    case '3':
                        echo "D";
                        break;
                    case '2':
                        echo "C";
                        break;
                    case '1':
                        echo "B";
                        break;
                    case '0':
                        echo "A";
                        break;
                        break;
                }

                for ($j=0; $j <= 3; $j++) {

                    //este if muestra las imagenes de las butacas con colores (comprada en gris, libre en blanco, ocupada en rojo)

                    //si la butaca se encuentra en el array de ocupadasRandom quiere decir que esta ocupada

                    if($butaca[$j][$i] == $ocupadasRandom[0] || $butaca[$j][$i] == $ocupadasRandom[1] || $butaca[$j][$i] == $ocupadasRandom[2] || $butaca[$j][$i] == $ocupadasRandom[3]){
                        //ocupada
                        print "<img src='/images/butacas/butaca_ocupada.png'>";

                        //si la butaca no esta ocupada y es la misma que has comprado entonces se compra
                    }elseif ($butaca[$j][$i] == $comprado[$nombre]) {
                        //comprada
                        print "<img src='/images/butacas/butaca_seleccionada.png'>";

                        //si la butaca no esta ocupada y no la has comprado entonces sigue esta libre
                    }else{
                        //libre
                        print "<img src='/images/butacas/butaca_vacia.png'>";
                    }
                }
                echo "<br>";
            }
            //este echo es para escribir los numeros que sirven como coordenadas
            echo "<pre style='font-size:16px'>    1    2    3    4 </pre>";
            echo "<br>";

            $hora=$_GET['hora'];

            //este echo dice quien y que butaca ha comprado
            foreach ($comprado as $key => $value) {
                echo $key.resultadoCompra.$value." para el día ".$hora;
            }

            echo "<br>";
            echo "<br>";

            //este echo dice cuantas palomitas y refrescos has comprado
            /*print_r($comida);*/

            $cuatro = array_pop($comida);
            $tres = array_pop($comida);
            $dos = array_pop($comida);
            $uno = array_pop($comida);


            if ($tres!=0&&$cuatro!=0){
                echo "Has comprado ".$tres." ".$uno." y ".$cuatro." ".$dos.".";
            }elseif($tres>0){
                echo "Has comprado ".$tres." ".$uno.".";
            }elseif($cuatro>0){
                echo "Has comprado ".$cuatro." ".$dos.".";
            }


            $precioTotal=$cantidadBebidaTotal+$cantidadPalomitaTotal+5.67-$descuentoTotal;

            echo "<br>";
            echo "<br>";
            echo "El precio total ha sido ".$precioTotal."€";

        }

        ?>
        </div>
    </div>
</div>
@endsection
