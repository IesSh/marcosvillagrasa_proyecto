@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
                @foreach($peliculas as $pelicula)
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a href="{{ route('peliculas.details',$pelicula) }}">
                                    <img class="img-thumbnail" src="images/{{ $pelicula->imagen }}" style="width: 235px"/>
                                </a>
                            </div>
                            <div class="col-lg-9">
                                <h1 class="col-lg-12">{{$pelicula->titulo}}</h1>
                                <h5 class="col-lg-2">{{$pelicula->genero}}</h5>
                                <p class="col-lg-12"> {{$pelicula->sinopsis}}</p>
                                <div class="col-lg-12">
                                    <a class="btn btn-primary" href="{{ route('peliculas.details',$pelicula) }}">Detalles</a>
                                </div>
                            </div>

                        </div>
                        <hr>
                    </div>
                @endforeach

        </div>
    </div>
@endsection
