@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-12">
                <div class="col-12 card mb-4" style="background-color: #b0c8f3">
                    <a class="navbar-brand" href="{{ url('/peliculas') }}" style="color: black">
                        Películas
                    </a>
                </div>
            </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <img class="img-thumbnail" src="{{ URL::to('images') }}/{{ $peliculas->imagen }}"/>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <h1 class="col-lg-12">{{$peliculas->titulo}}</h1>
                        <div class="col-lg-12">
                            <div class="row">
                                <h5 class="col-lg-1">Genero: </h5>
                                <h5 class="col-lg-1">{{$peliculas->genero}}</h5>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <h5 class="col-lg-1">Idioma: </h5>
                                <h5 class="col-lg-2">{{$peliculas->idioma}}</h5>
                            </div>
                        </div>
                    </div>
                        <p>
                            {{$peliculas->sinopsis}}
                        </p>
                        <iframe width="560" height="315" src="{{$peliculas->trailer}}" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    </div>
                </div>
                <div class="col-12">
                    <hr>
                    <div class="row justify-content-center">
                            <a class="btn btn-primary navbar-brand" href="{{ route('peliculas.buy',$peliculas) }}">
                                Comprar entrada
                            </a>
                    </div>
                </div>
        </div>
    </div>

@endsection
