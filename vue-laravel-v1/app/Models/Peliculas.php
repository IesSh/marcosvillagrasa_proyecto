<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peliculas extends Model
{
   // use HasFactory;

    protected $fillable = [
        'id',
        'titulo',
        'genero',
        'trailer',
        'horas',
        'idioma',
        'sinopsis',
        'imagen',
    ];
/*
    public function scopeTitulo($query,$titulo){
        if ($titulo){
            return $query->where('titulo','like',"%$titulo%");
        }
    }*/

    public function scopeBuscarpor($query,$tipo,$buscar){
        if (($tipo) && ($buscar)){
            return $query->where($tipo,'like',"%$buscar%");
        }
    }

}
