<?php

namespace App\Http\Controllers;

use App\Models\Peliculas;
use Illuminate\Http\Request;

class AdministradorController extends Controller
{
    //
    public function __construct(){

        $this->middleware('EsAdmin');


    }

    public function create(){
        $peliculas = Peliculas::all();
        return view('peliculas.create',compact('peliculas'));

    }

    public function store(Request $request){
        /*
                $peliculas=new Peliculas;

                $peliculas->titulo=$request->titulo;
                $peliculas->genero=$request->genero;
                $peliculas->sinopsis=$request->sinopsis;
                $peliculas->horas=$request->horas;
                $peliculas->idioma=$request->idioma;
                $peliculas->trailer=$request->trailer;
        */
        $entrada=$request->all();

        if ($archivo=$request->file('imagen')){

            $nombre=$archivo->getClientOriginalName();

            $archivo->move('images',$nombre);

            $entrada['imagen']=$nombre;
        }

        Peliculas::create($entrada);

        //$peliculas->save();

        return back()->with('mensaje', 'Pelicula creado con exito');

    }

    public function update(Request $request, $id){

        $peliculas = Peliculas::findOrFail($id);

        $peliculas->update($request->all());


        return redirect('/peliculas');

    }

    public function edit($id){

        $peliculas = Peliculas::findOrFail($id);

        return view('peliculas.edit',compact('peliculas'));

    }
    public function destroy($id){

        $peliculas = Peliculas::findOrFail($id);

        $peliculas->delete();

        return redirect('/peliculas');

    }
}
