<?php

namespace App\Http\Controllers;

use App\Models\Peliculas;

use App\Models\Horas;

use Illuminate\Http\Request;

class PeliculasController extends Controller
{
    //
    public function index(Request $request){

        $buscar = $request->get('buscador');

        $tipo= $request->get('tipo');


        $peliculas = Peliculas::buscarpor($tipo, $buscar)->get();

       // $peliculas = Peliculas::all();
        return view('peliculas.index',compact('peliculas'));
    }

    public function popular(){
        $peliculas = Peliculas::paginate(5);
        return view('peliculas.popular',compact('peliculas'));
    }


    public function parametres(Request $request, $id)
    {
        $peliculas = Peliculas::findOrFail($id);

        $horas = Horas::all();

        return view('peliculas.details',compact('peliculas','horas'));
    }


    public function buy(Request $request, $id)
    {
        $peliculas = Peliculas::findOrFail($id);

        $horas = Horas::all();
        return view('peliculas.buy',compact('peliculas','horas'));


    }
    public function ticket(Request $request, $id)
    {
        $peliculas = Peliculas::findOrFail($id);
        return view('peliculas.ticket',compact('peliculas'));


    }

}
