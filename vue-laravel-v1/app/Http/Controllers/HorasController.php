<?php

namespace App\Http\Controllers;

use App\Models\Horas;
use Illuminate\Http\Request;

class HorasController extends Controller
{
    public function index(){

        $horas = Horas::all();

        return view('peliculas.details',compact('horas'));
    }
}
